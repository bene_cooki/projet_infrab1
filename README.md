## **__Prérequis__**
- Pour réaliser notre projet il va nous falloir:
- une machine sous debian 10(de préférence)
- hostname défini
- ssh fonctionnel
- accès internet
- un espace stockage d'au moins 5 Go

## User
### Créer un utilisateur
Pour créer un nouvel utilisateur
```
sudo adduser "nom-utilisateur"
```
### Donné les permition sudo
Ajouter l'utilisateur "nom-utilisateur" au groupe "sudo".
```
usermod -aG sudo "nom-utilisateur"
```
Petite précision : l'option -aG permet d'ajouter l'utilisateur "nom-utilisateur" au groupe "sudo", mais l'aurez deviné ! On peut vérifier que le résultat de notre commande est correct en regardant de quels groupes est membre l'utilisateur :
```
groups "nom-utilisateur"
```
Autoriser l’utilisateur à exécuter des commandes sudo sans qu’un mot de passe lui soit demandé. Pour ce faire, ouvrez le fichier /etc/sudoers :
```
visudo
```
Faites défiler jusqu’à la fin du fichier et ajoutez la ligne suivante :
```
"nom-utilisateur" ALL=(ALL) NOPASSWD:ALL
```
Enregistrez un fichier et quittez l’éditeur. N’oubliez pas de remplacer “nom d’utilisateur” par le nom d’utilisateur auquel vous souhaitez accorder l’accès.

## Installe Docker

Exécuter  les commande suivante:
```
curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

```
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/local/bin/docker-compose
```


## Installe fai2ban

### 1.Installation

Installez le paquet fail2ban disponible dans les dépôts Universe.
Il convient ensuite de lancer le service fail2ban

```
systemctl start fail2ban
```

Puis d'en créer le démarrage automatique

```
systemctl enable fail2ban
```

Et enfin de contrôler la bonne installation

```
systemctl status fail2ban
```

Si la réponse comporte du rouge et le mot "failed" " sur la ligne commençant par "Active :", les dernières lignes du message indiquent les raisons de l'échec et permettent sa correction avant un nouvel essai, à tenter après lecture du reste de cet article.

Si la réponse comporte du vert et les mots "active (running)" sur la ligne commençant par "Active :", le service est installé et actif.

### 2.Configuration

2.1 Paramétrage par défaut

Les paramètres par défaut sont visibles dans le fichier /etc/fail2ban/jail.conf

La durée de bannissement d'une IP est définie par la directive bantime avec une valeur en secondes. La valeur par défaut est de 600 s, soit 10 minutes, ce qui est beaucoup trop court. Il est plus réaliste d'avoir des durées de bannissement d'une ou plusieurs heures, voir plusieurs jours.

```
sudo nano /etc/fail2ban/jail.d/custom.conf
```

2.2 Configurer fail2ban pour les services actifs

Pour spécifier à fail2ban quels services il doit surveiller, il faut activer les « jails » (prisons) correspondantes.

```
sudo nano /etc/fail2ban/jail.conf
```

Relancez la configuration avec :

```
sudo systemctl restart fail2ban
```

### 3.Gestion du bannissement

3.1 Voir le status des jails
Pour savoir si votre jail est actif, vous devriez le voir affiché, après avoir taper cette commande :

```
sudo fail2ban-client status
```

Pour savoir si une de vos jails de votre fail2ban a bannis une ou plusieurs IP, taper cette commande :

```
sudo fail2ban-client status [Nom du jail]
```

3.2 Dé-bannir une IP de l'un de vos jails

Une de vos adresse IP se retrouve blacklisté suite à une mauvaise manips répété ou un test de sécurité. Vous pouvez le retirer de la liste des IP blacklisté de fail2ban avec cette commande :
```
sudo fail2ban-client set [nom du jail] unbanip [IP concerné]
```

## config clé ssh

```
ssh-keygen
jesuisaynov
ls ~/.ssh
```
```
cat ~/.ssh/rsa.pub | ssh utilisateur@51.75.206.213 "cat >> ~/.ssh/authorized_keys"
```
## sécu
Pour configurer le ssh
```
sudo nano /etc/ssh/sshd_config
```
Il faut modifier ça :
```
+-----------------------+
|PORTSSH = 22           |
|PermitRootLogin = Yes  |
+-----------------------+
```
Par ça :
```
+-----------------------+
|PORTSSH = 55555        |
|PermitRootLogin = No   |
+-----------------------+
```
Pour relancer le service sans devoir reboot le serveur pour evité de deconnéter les client :
```
service ssh restart
systemctl restart sshd
```
## server minecraft

Installation de java : 

```
sudo apt install openjdk-11-jdk
```

Installation de sed :

```
sudo apt install sed
```
```
sudo apt install seb
```

créer a la racine du server ( cd /) cette Arboraissance :

```
 mkdir serverminecraft
 mkdir /serverminecraft/config_server
 mkdir /serverminecraft/config_server/server_open
 mkdir /serverminecraft/config_server/server_created
 mkdir /serverminecraft/config_server/server_generator
```
Ensuite : 

```
sudo touch /serverminecraft/config_server/ports_used.txt
sudo nano /serverminecraft/config_server/server_created/start.sh
chmod +x /serverminecraft/config_server/server_created/start.sh
```

copié dans start.sh :

```
#!/usr/bin/bash

if [[ $1 == 'help' ]]; then
        echo ' Create : Create an New MINECRAFT SERVER \n'
        exit
fi

read -p "[ Nom du Server ] :" NAME_SERVER
#read -p "[ Gamemod ] :" NAME_GAMEMOD

if [[ $1 != 'create' && $1 != 'start' ]]; then
        echo ' Error command unknow, Use help for more information\n'
        exit
fi

if [[ $1 == 'create' ]]; then
        echo $NAME_SERVER
        mkdir /serverminecraft/config_server/server_open/$NAME_SERVER
        cp /serverminecraft/config_server/server_generator/* . -r
        for i in {0..65521}; do
                cat ./../ports_used.txt | grep $i
                if [[ $? != '0' ]]; then
                        echo ''$i' not Used'
                        echo $i >> ./../ports_used.txt
                        break
                else
                        echo ''$i' Used'
                continue
        fi
done
        sed -i -e 's/25565/'$i'/g' ./server.properties
        java -Xmx512M -Xms512M -jar server.jar nogui &
        echo 'server OUVERT\nIP : 176.31.163.20.'$i''
        exit
fi


if [[ $1 == 'start' ]]; then
        cp -r $(ls | grep -v start.sh) ./../server_open/$NAME_SERVER/
        rm -r $(ls | grep -v start.sh)
        exit
fi
```

et pour terminé dans server_generator on va y mettre les fichiers importants :

```
cd /serverminecraft/config_server/server_generator

sudo wget https://launcher.mojang.com/v1/objects/a0d03225615ba897619220e256a266cb33a44b6b/server.jar

java -Xmx512M -Xms512M -jar server.jar nogui 
(le fichier eula.txt n'est pas accépté ducoup il ne lanceras pas de server)

sudo nano eula.txt 

et modifié false en true 
```

Maintentant pour lancer un server il vous faudras :

```
cd /serverminecraft/config_server/server_created
sudo ./start.sh (created pour create le server)
sudo ./start.sh start (pour le déplacé )
```

